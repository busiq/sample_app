#= require_self
#= require hamlcoffee
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views

window.SimpleTodo =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  
  initialize: ->
    controller = $('body').data('controller')
    if SimpleTodo.Views[controller]?
      @view = new SimpleTodo.Views[controller]()
$ ->
  SimpleTodo.initialize()