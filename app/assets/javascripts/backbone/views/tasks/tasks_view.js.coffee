class SimpleTodo.Views.Tasks extends Backbone.View
  template: JST["backbone/templates/tasks/index"]
  el: '#tasks'
  
  events:
    'click #addTask' : 'addTask'
    'click #removeDone' : 'removeDone'

  initialize: ->
    $('container')
    @collection = new SimpleTodo.Collections.Tasks()
    @collection.fetch({async: false})
    @collection.on('change add destroy', @render, @)
    @render()
    
  addTask: ->
    input = $('#task_caption').val()
    if input.length > 0
      @collection.create({caption: input})

  addAll: ->
    that = @
    @collection.each (task)->
      that.addOne(task)

  addOne: (task)->
    view = new SimpleTodo.Views.Task({model : task})
    @$el.find('ul').append(view.render().el)
    
  removeDone: ->
    alert('I am not implemented yet...')

  render: ->
    $(@el).html(@template())
    @addAll()
    return this
